function display(update) {
    const updates = `<div class="col-lg-12">
                <div class="card updates-cards">
                    <div class="card-body">
                        <h4 class="card-header">${update.content[0].title}</h4>
                        <div class="scrollbar">
                        <div class="text">  
                        <br>
                        <div class="text_title"><i class="fa fa-file-text" aria-hidden="true"></i> ${update.content[0].posts[0].title}</div>
                        <div class="text_date_time">${update.content[0].posts[0].date_time}</div>
                        <div class="short_description">${update.content[0].posts[0].short_description}</div>
                        <div class="description">${update.content[0].posts[0].description} <br></p>
                        <br>
                        </div></div>
                        <br>
                        <div class="video">
                        <br>
                        <div class="video_title"><i class="fa fa-video-camera" aria-hidden="true"></i> ${update.content[0].posts[1].title}</div>
                        <div class="video_date_time">${update.content[0].posts[1].date_time}</div>
                        <p class="short_description">${update.content[0].posts[1].short_description}</p>
                        <div class="update_video">
                        <video controls><source src="${update.content[0].posts[1].video_path}" type="video/mp4">
                        </video>
                        </div>
                        <br>
                        </div>
                        <br>
                        <div class="file">
                        <br>
                        <div class="file_title"><i class="fa fa-folder" aria-hidden="true"></i> ${update.content[0].posts[2].title}</div>
                        <div class="file_date_time"> ${update.content[0].posts[2].date_time}</div>
                        <p class="short_description"> ${update.content[0].posts[2].short_description}</p>
                        <a href="${update.content[0].posts[2].file_path}" class="btn btn-1" id="download-btn">Download</a>
                        <br>
                        <br>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
 
            <div class="col-lg-12">
                <div class="card updates-cards">
                    <div class="card-body">
                        <h4 class="card-header">${update.content[1].title}</h4>
                        <i data-bs-toggle="collapse" data-bs-target="#collapse_1" aria-expanded="false" aria-controls="collapse_1" class="fa fa-chevron-down  topic_arrow" aria-hidden="true"></i>       
                        <div class="collapse" id="collapse_1">
                        <div class="scrollbar">
                        <div class="text">  
                        <br>
                        <div class="text_title" ><i class="fa fa-file-text" aria-hidden="true"></i> ${update.content[1].posts[0].title}</div>
                        <div class="text_date_time">${update.content[1].posts[0].date_time}</div> 
                <div class="short_description">${update.content[1].posts[0].short_description}</div>
                <i data-bs-toggle="collapse" data-bs-target="#text_1" aria-expanded="false" aria-controls="text_1" class="fa fa-chevron-down" aria-hidden="true"></i>       
                <div class="collapse" id="text_1">
                        <div class="description">${update.content[1].posts[0].description} <br></p>
                        <br>
                        </div></div>
                        </div>
                        <br>
                        <div class="video">
                        <br>
                        <div class="text_title" ><i class="fa fa-video-camera" aria-hidden="true"></i> ${update.content[1].posts[1].title}</div>
                        <div class="text_date_time">${update.content[1].posts[1].date_time}</div> 
                        <div class="short_description">${update.content[1].posts[1].short_description}</div>
                        <i data-bs-toggle="collapse" data-bs-target="#video_1" aria-expanded="false" aria-controls="video_1" class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="collapse" id="video_1">
                        <div class="update_video">
                        <video controls><source src="${update.content[1].posts[1].video_path}" type="video/mp4">
                        </video>
                        </div>
                        <br>
                        </div></div>
                        <br>

                        <div class="file">
                        <br>
                        <div class="text_title" ><i class="fa fa-folder" aria-hidden="true"></i> ${update.content[1].posts[2].title}</div>
                        <div class="text_date_time">${update.content[1].posts[2].date_time}</div> 
                <div class="short_description">${update.content[1].posts[2].short_description}</div>
                        <i data-bs-toggle="collapse" data-bs-target="#file_1" aria-expanded="false" aria-controls="file_1" class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="collapse" id="file_1">
                        <a href="${update.content[1].posts[2].file_path}" class="btn btn-1" id="download-btn">Download</a>
                        <br>
                        <br>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>     
            </div>       
            
            
       
            <div class="col-lg-12">
                <div class="card updates-cards">
                    <div class="card-body">
                        <h4 class="card-header">${update.content[2].title}</h4>
                        <i data-bs-toggle="collapse" data-bs-target="#collapse_1" aria-expanded="false" aria-controls="collapse_1" class="fa fa-chevron-down  topic_arrow" aria-hidden="true"></i>       
                        <div class="collapse" id="collapse_1">
                        <div class="scrollbar">
                        <div class="text">  
                        <br>
                        <div class="text_title" ><i class="fa fa-file-text" aria-hidden="true"></i> ${update.content[2].posts[0].title}</div>
                        <div class="text_date_time">${update.content[2].posts[0].date_time}</div> 
                <div class="short_description">${update.content[2].posts[0].short_description}</div>
                <i data-bs-toggle="collapse" data-bs-target="#text_1" aria-expanded="false" aria-controls="text_1" class="fa fa-chevron-down" aria-hidden="true"></i>       
                <div class="collapse" id="text_1">
                        <div class="description">${update.content[2].posts[0].description} <br></p>
                        <br>
                        </div></div>
                        </div>
                        <br>
                        <div class="video">
                        <br>
                        <div class="text_title" ><i class="fa fa-video-camera" aria-hidden="true"></i> ${update.content[2].posts[1].title}</div>
                        <div class="text_date_time">${update.content[2].posts[1].date_time}</div> 
                        <div class="short_description">${update.content[2].posts[1].short_description}</div>
                        <i data-bs-toggle="collapse" data-bs-target="#video_1" aria-expanded="false" aria-controls="video_1" class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="collapse" id="video_1">
                        <div class="update_video">
                        <video controls><source src="${update.content[2].posts[1].video_path}" type="video/mp4">
                        </video>
                        </div>
                        <br>
                        </div></div>
                        <br>

                        <div class="file">
                        <br>
                        <div class="text_title" ><i class="fa fa-folder" aria-hidden="true"></i> ${update.content[2].posts[2].title}</div>
                        <div class="text_date_time">${update.content[2].posts[2].date_time}</div> 
                <div class="short_description">${update.content[2].posts[2].short_description}</div>
                        <i data-bs-toggle="collapse" data-bs-target="#file_1" aria-expanded="false" aria-controls="file_1" class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="collapse" id="file_1">
                        <a href="${update.content[2].posts[2].file_path}" class="btn btn-1" id="download-btn">Download</a>
                        <br>
                        <br>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>     
            </div>       


            
            <div class="col-lg-12">
                <div class="card updates-cards">
                    <div class="card-body">
                        <h4 class="card-header">${update.content[3].title}</h4>
                        <i data-bs-toggle="collapse" data-bs-target="#collapse_1" aria-expanded="false" aria-controls="collapse_1" class="fa fa-chevron-down  topic_arrow" aria-hidden="true"></i>       
                        <div class="collapse" id="collapse_1">
                        <div class="scrollbar">
                        <div class="text">  
                        <br>
                        <div class="text_title" ><i class="fa fa-file-text" aria-hidden="true"></i> ${update.content[3].posts[0].title}</div>
                        <div class="text_date_time">${update.content[3].posts[0].date_time}</div> 
                <div class="short_description">${update.content[3].posts[0].short_description}</div>
                <i data-bs-toggle="collapse" data-bs-target="#text_1" aria-expanded="false" aria-controls="text_1" class="fa fa-chevron-down" aria-hidden="true"></i>       
                <div class="collapse" id="text_1">
                        <div class="description">${update.content[3].posts[0].description} <br></p>
                        <br>
                        </div></div>
                        </div>
                        <br>
                        <div class="video">
                        <br>
                        <div class="text_title" ><i class="fa fa-video-camera" aria-hidden="true"></i> ${update.content[3].posts[1].title}</div>
                        <div class="text_date_time">${update.content[3].posts[1].date_time}</div> 
                        <div class="short_description">${update.content[3].posts[1].short_description}</div>
                        <i data-bs-toggle="collapse" data-bs-target="#video_1" aria-expanded="false" aria-controls="video_1" class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="collapse" id="video_1">
                        <div class="update_video">
                        <video controls><source src="${update.content[3].posts[1].video_path}" type="video/mp4">
                        </video>
                        </div>
                        <br>
                        </div></div>
                        <br>

                        <div class="file">
                        <br>
                        <div class="text_title" ><i class="fa fa-folder" aria-hidden="true"></i> ${update.content[3].posts[2].title}</div>
                        <div class="text_date_time">${update.content[3].posts[2].date_time}</div> 
                <div class="short_description">${update.content[3].posts[2].short_description}</div>
                        <i data-bs-toggle="collapse" data-bs-target="#file_1" aria-expanded="false" aria-controls="file_1" class="fa fa-chevron-down" aria-hidden="true"></i>
                        <div class="collapse" id="file_1">
                        <a href="${update.content[3].posts[2].file_path}" class="btn btn-1" id="download-btn">Download</a>
                        <br>
                        <br>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>     
            </div>       
        `;
    document.querySelector('.updates-row').innerHTML = updates;
}

const xhr = new XMLHttpRequest();

xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
        display(JSON.parse(xhr.responseText));
    }
}
xhr.open('GET', 'https://vidura-websites.s3.ap-southeast-1.amazonaws.com/thamira/thamira_class_latest_feed.json');
xhr.send();
